class Validations {
    /**
     * Create validation instance
     *
     * @param errorHandle
     * @param rules
     */
    constructor(errorHandle, rules) {
        this.errorHandle = errorHandle;
        this.rules = rules;
        this.data = {};
        this.errors = {};
    }

    /**
     * Check form fields/data with validation
     *
     * @param data
     */
    check(data) {
        this.errorHandle.clear();
        this.data = data;
        this.errors = {};
        for (let rule of this.rules) {
            let [key, value] = Object.entries(rule)[0] ?? [
                null,
                null
            ];
            let rule_array = value.split("|");
            for (let sub_rule of rule_array) {
                let [func, param] = sub_rule.split(":");
                if (typeof this[func.trim()] !== "undefined") {
                    this[func.trim()](key, param);
                }
            }
        }
        this.errorHandle.record(this.errors);
    }

    /**
     * Replace key with attribute title
     *
     * @param key
     * @returns {*}
     */
    keyToName(key) {
        return key.replace(/_/g, ' ');
    }

    /**
     * Validation => Required
     *
     * @param key
     * @param param
     */
    required(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            if (this.data[key] == null || this.data[key].toString().trim() === '' || this.data[key] === false) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' field is required.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' field is required.');
            }
        }
    }

    /**
     * Validation => Email Address
     *
     * @param key
     * @param param
     */
    email(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' must be a valid email address.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' must be a valid email address.');
            }
        }
    }

    /**
     * Validation => Numeric only
     * @param key
     * @param param
     */
    numeric(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /^[0-9]+$/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' must be a number.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' must be a number.');
            }
        }
    }

    /**
     * Validation => Alphabets only
     *
     * @param key
     * @param param
     */
    alpha(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /^[a-zA-Z]+$/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' may only contain letters.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' may only contain letters.');
            }
        }
    }

    /**
     * Validation => Alphabets and spaces only
     *
     * @param key
     * @param param
     */
    alpha_space(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /^[a-zA-Z\s]+$/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' may only contain space & letters.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' may only contain space & letters.');
            }
        }
    }

    /**
     * Validation => Alphabets and Numeric only
     *
     * @param key
     * @param param
     */
    alpha_numeric(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /^[a-zA-Z0-9]+$/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' may only contain numbers & letters.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' may only contain numbers & letters.');
            }
        }
    }

    /**
     * Validation => Alphabet, Numeric And Spaces only
     *
     * @param key
     * @param param
     */
    alpha_numeric_space(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /^[a-zA-Z0-9\s]+$/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' may only contain space, numbers & letters.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' may only contain space, numbers & letters.');
            }
        }
    }

    /**
     * Validation => Min length
     *
     * @param key
     * @param param
     */
    min(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && this.data[key] != null && this.data[key].length < param) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' must be at least ' + param + ' characters.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' must be at least ' + param + ' characters.');
            }
        }
    }

    /**
     * Validation => Max length
     *
     * @param key
     * @param param
     */
    max(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && this.data[key].length > param) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' may not be greater than ' + param + ' characters.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' may not be greater than ' + param + ' characters.');
            }
        }
    }

    /**
     * Validation => Should contain at least one lower case
     * @param key
     * @param param
     */
    containLowerCase(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /[a-z]/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' contains at least 1 lowercase letter')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' contains at least 1 lowercase letter');
            }
        }
    }

    /**
     * Validation => Should contain at least one upper case
     * @param key
     * @param param
     */
    containUpperCase(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /[A-Z]/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' must contains at least 1 uppercase letter')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' must contains at least 1 uppercase letter');
            }
        }
    }

    /**
     * Validation => Should contain at least one number
     *
     * @param key
     * @param param
     */
    containNumber(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /[0-9]/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' must contains at least 1 numeric letter')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' must contains at least 1 numeric letter');
            }
        }
    }

    /**
     * Validation => Should contain at least one special case
     *
     * @param key
     * @param param
     */
    containSpecial(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = /[@$!%*#?&]/;
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' must contains at least 1 special character')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' must contains at least 1 special character');
            }
        }
    }

    /**
     * Validation => Should be same two fields
     *
     * @param key
     * @param param
     */
    same(key, param = null) {
        if (typeof this.data[key] !== "undefined" && typeof this.data[param] !== "undefined") {
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && this.data[key] != this.data[param]) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' & ' + this.keyToName(param) + ' must be same.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' & ' + this.keyToName(param) + ' must be same.');
            }
        }
    }

    /**
     * Validation => Two fields should different
     *
     * @param key
     * @param param
     */
    different(key, param = null) {
        if (typeof this.data[key] !== "undefined" && typeof this.data[param] !== "undefined") {
            if (this.data[key] != null && this.data[key].toString().trim() !== '' && this.data[key] == this.data[param]) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' & ' + this.keyToName(param) + ' must be different.')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' & ' + this.keyToName(param) + ' must be different.');
            }
        }
    }

    /**
     * Validation => Should be a valid url
     *
     * @param key
     * @param param
     */
    url(key, param = null) {
        if (typeof this.data[key] !== "undefined") {
            let expression = new RegExp('^(https?:\\/\\/)' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator

            if (this.data[key] != null && this.data[key].toString().trim() !== '' && !expression.test(this.data[key])) {
                if (!this.errors[key])
                    this.errors[key] = [('The ' + this.keyToName(key) + ' format is invalid, Expected format: http://www.example.com')];
                else
                    this.errors[key].push('The ' + this.keyToName(key) + ' format is invalid, Expected format: http://www.example.com');
            }
        }
    }
}

export default Validations;
