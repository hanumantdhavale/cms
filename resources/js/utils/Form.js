import Errors from './Errors';
import Validations from "./Validations";

class Form {
    /**
     * Create a new Form instance.
     *
     * @param data
     * @param rules
     */
    constructor(data, rules = [], is_form_data = true) {
        this.server_response = {};
        this.busy = false;
        this.originalData = data;
        for (let field in data) {
            this[field] = data[field];
        }
        this.is_form_data = is_form_data;
        this.errors = new Errors();
        this.validations = new Validations(this.errors, rules);
        this.autoFocus();
    }

    /**
     * Autofocus on 1st form input
     */
    autoFocus() {
        setTimeout(() => {
            let forms = document.getElementsByTagName('form');
            for (let form of forms) {
                for (let ele of form) {
                    $(ele).focus();
                    if (ele.autofocus)
                        break;
                }
            }
        }, 50);
    }

    /**
     * Fetch all relevant data for the form.
     *
     * @returns {{}}
     */
    data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }
        return data;
    }

    /**
     * set all relevant data for the form.
     *
     * @param new_data
     * @returns {{}}
     */
    setData(new_data) {
        let data = {};
        for (let property in this.originalData) {
            this[property] = new_data[property] ?? this.originalData[property];
        }
        return data;
    }

    /**
     * Reset the form fields.
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }

        this.errors.clear();
    }

    /**
     * Send a POST request to the given URL.
     *
     * @param url
     * @returns {undefined|Promise<unknown>}
     */
    post(url, clear = true) {
        return this.submit('post', url, clear);
    }


    /**
     * Send a PUT request to the given URL.
     *
     * @param url
     * @returns {undefined|Promise<unknown>}
     */
    put(url, clear = true) {
        return this.submit('put', url, clear);
    }


    /**
     * Send a PATCH request to the given URL.
     *
     * @param url
     * @returns {undefined|Promise<unknown>}
     */
    patch(url, clear = true) {
        return this.submit('patch', url, clear);
    }

    /**
     * Send a DELETE request to the given URL.
     *
     * @param url
     * @returns {undefined|Promise<unknown>}
     */
    delete(url, clear = true) {
        return this.submit('delete', url, clear);
    }

    /**
     * Validate form data
     */
    validate() {
        this.validations.check(this.data());
    }

    /**
     * Returns data in form of FormData() format
     *
     * @returns {FormData}
     */
    formData() {
        const form = new FormData();
        let data = this.data();
        Object.keys(data).forEach((key) => {
            let value = data[key] ?? '';
            form.append(key, value);
        });
        return form;
    }

    /**
     * Submit the form.
     *
     * @param requestType
     * @param url
     * @returns {Promise<unknown>}
     */
    submit(requestType, url, clear = true) {
        let config;
        let form;
        if (this.is_form_data) {
            form = this.formData();
            if (requestType === 'put' || requestType === 'patch') {
                form.append('_method', requestType);
                requestType = 'post';
            }
            config = {headers: {'Content-Type': 'multipart/form-data'}};
        } else {
            form = this.data();
            config = {};
        }
        this.validate();
        return new Promise((resolve, reject) => {
            if (this.errors.any()) {
                reject('Validation error on frontend.');
                return 0;
            }
            this.busy = true;
            axios[requestType](url, form, config).then(response => {
                if (clear == 'true')
                    this.onSuccess(response.data);
                if (response.data.message) {
                    this.errors.status = 'success';
                    this.errors.message = response.data.message;
                }
                if (response.data.next) {
                    setTimeout(() => {
                        window.location = response.data.next;
                    }, response.data.delay ?? 1);
                }
                this.busy = false;
                this.server_response = response.data;
                resolve(response.data);
            }).catch(error => {
                if (error.response && error.response.data && error.response.data.message) {
                    this.errors.status = 'error';
                    this.errors.message = error.response.data.message;
                }
                if (error.response && error.response.status == 422) {
                    this.onFail(error.response.data.errors);
                } else if (error.response && error.response.status == 419 && error.response.data.message == 'CSRF token mismatch.') {
                    window.location.reload();
                } else if (error.response && error.response.status == 400 && error.response.data.next) {
                    window.location = error.response.data.next;
                }
                this.busy = false;
                this.errors.server = error;
                reject(error);
            });
        });
    }


    /**
     * Handle a successful form submission.
     *
     * @param data
     */
    onSuccess(data) {
        this.reset();
    }


    /**
     * Handle a failed form submission.
     *
     * @param errors
     */
    onFail(errors) {
        this.errors.record(errors);
    }
}

window.vForm = Form;
