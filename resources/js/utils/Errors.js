class Errors {
    /**
     * Create a new Errors instance.
     */
    constructor() {
        this.server = null;
        this.errors = {};
        this.status = null;
        this.message = null;
    }


    /**
     * Determine if an errors exists for the given field.
     *
     * @param field
     * @returns {boolean}
     */
    has(field) {
        return this.errors.hasOwnProperty(field);
    }


    /**
     * Determine if we have any errors.
     *
     * @returns {boolean}
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }

    /**
     * Retrieve the error message for a field.
     *
     * @param field
     * @returns {*}
     */
    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }

    /**
     * Retrieve all error messages for a field.
     *
     * @param field
     * @returns {*}
     */
    getAll(field) {
        if (this.errors[field]) {
            return this.errors[field];
        }
    }

    /**
     * Record the new errors.
     *
     * @param errors
     */
    record(errors) {
        this.errors = errors;
        this.scrollBehavior();
    }

    /**
     * Clear one or all error fields.
     *
     * @param field
     */
    clear(field) {
        if (field) {
            delete this.errors[field];
            return;
        }
        this.errors = {};
        this.server = null;
        this.status = null;
        this.message = null;
    }

    /**
     * Scroll to error message
     */
    scrollBehavior() {
        setTimeout(() => {
            let forms = document.getElementsByTagName('form');
            let selected_ele = null;
            for (let form of forms) {
                for (let ele of form) {
                    if ($(ele).hasClass('kp-invalid')) {
                        selected_ele = ele;
                        break;
                    }
                }
            }
            let first_top_pos = $(forms[0][0]).offset().top < 0 ? $(forms[0][0]).offset().top * (-1) : $(forms[0][0]).offset().top;
            if (selected_ele) {
                let this_position = $(selected_ele).offset().top < 0 ? $(selected_ele).offset().top * (-1) : $(selected_ele).offset().top;
                let scrollOffset = first_top_pos - this_position + $(selected_ele).parent().outerHeight();
                scrollOffset = scrollOffset < 0 ? scrollOffset * (-1) : scrollOffset;
                $('.kp-main-container').animate({
                    scrollTop: scrollOffset
                }, 2000);
            }
        }, 100);
    }
}

export default Errors;
