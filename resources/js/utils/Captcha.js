class Captcha {
    constructor(object) {
        this.el = object.el;
        this.captchId = null;
        this.captchaString = null;
        this.userInput = null;
        this.captchID();
        this.refresh();
        this.createCaptch();
    }

    createCaptch() {
        let captch = document.getElementById(this.el);
        let captchaHtml = `
            <div>
                <div style="display: flex;flex-wrap: wrap;align-content: center;align-items: center;">
                    <div id="captchText-${this.captchId}" style="display: inline-flex;background: #fff;border: 1px solid #999;margin: 0 10px 5px 0;">
                        <canvas id="captchCanvas-${this.captchId}" width = "136" height = "22"></canvas>
                    </div>
                    <input type="text" id="textInput-${this.captchId}" style="width:auto;display:inline-block;border: 1px solid #999;padding: 5px;margin: 0 10px 5px 0;outline: none;">
                    <div style="margin: 0 0 5px 0;"><a href="JavaScript:void(0)" id="captchHandle-${this.captchId}" style="background: #f15b36;color: #fff;border: #f15b36;outline: none;border-radius: 3px;padding: 1px 5px;font-size: 19px;text-align: center;display: inline-block;text-decoration: none;">&#10227;</a></div>
                </div>
                <div id="error-${this.captchId}" style="font-size: 14px;color: #f15b36;padding: 3px 0 0 0;display: none;"></div>
            </div>`;
        captch.insertAdjacentHTML('beforeend', captchaHtml);
        let refreshHandler = document.getElementById(`captchHandle-${this.captchId}`);
        let textInput = document.getElementById(`textInput-${this.captchId}`);
        refreshHandler.addEventListener('click', () => {
            this.refresh();
            this.setCaptcha();
            this.resetError();
            textInput.value = null;
        });
        textInput.addEventListener('input', (e) => {
            this.resetError();
            this.userInput = e.target.value;
        });
        this.setCaptcha();
    }

    setCaptcha() {
        let captchCanvas = document.getElementById(`captchCanvas-${this.captchId}`);
        if (!captchCanvas.getContext) {
            console.log("Capcha not support on this browser.");
            return null;
        }
        let ctx = captchCanvas.getContext('2d');
        ctx.clearRect(0, 0, captchCanvas.width, captchCanvas.height);

        let grd = ctx.createLinearGradient(0, 0, captchCanvas.width, captchCanvas.height);
        let offset = this.rndInteger(0, 2);
        let offset1 = offset == 1 ? 0 : 1;
        grd.addColorStop(offset, "#eee");
        grd.addColorStop(offset1, "#aaa");
        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, captchCanvas.width, captchCanvas.height);

        this.drawNumber(ctx, captchCanvas.width, captchCanvas.height);
        this.drawLines(ctx);
    }

    drawNumber(ctx, w, h) {
        ctx.font = 'Italic 15px emoji';
        ctx.textBaseline = 'Top';
        ctx.textAlign = 'center';
        ctx.strokeText(this.captchaString, w / 2, 18);
    }

    rndInteger(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    rndColor() {
        let colors = ["#ff3e00", "#5fff00", "#00ffbb", "#0057ff", "#db00ff", "#ff0050", "#bdff00", "#6a317b", "#31517b", "#317b72"];
        return colors[this.rndInteger(0, 9)];
    }

    drawLines(ctx) {
        let x1 = this.rndInteger(0, 5);
        let y1 = this.rndInteger(2, 20);
        let x2 = this.rndInteger(124, 134);
        let y2 = this.rndInteger(2, 20);
        ctx.beginPath();
        ctx.moveTo(x1, y1);
        ctx.lineTo(x2, y2);
        ctx.closePath();
        ctx.strokeStyle = this.rndColor();
        ctx.stroke();

        let x3 = this.rndInteger(0, 5);
        let y3 = this.rndInteger(2, 20);
        let x4 = this.rndInteger(124, 134);
        let y4 = this.rndInteger(2, 20);
        ctx.beginPath();
        ctx.moveTo(x3, y3);
        ctx.lineTo(x4, y4);
        ctx.closePath();
        ctx.strokeStyle = this.rndColor();
        ctx.stroke();
    }

    captchID() {
        this.captchId = Math.floor(Math.random() * 999999) + 1;
    }

    refreshCaptcha() {
        let textInput = document.getElementById(`textInput-${this.captchId}`);
        this.refresh();
        this.setCaptcha();
        textInput.value = null;
    }

    refresh() {
        this.captchaString = Math.floor(Math.random() * 999999) + 1;
    }

    resetError() {
        let errorEl = document.getElementById(`error-${this.captchId}`);
        errorEl.innerText = null;
        errorEl.style.display = "none";
    }

    setError(error) {
        let errorEl = document.getElementById(`error-${this.captchId}`);
        errorEl.innerText = error;
        errorEl.style.display = "block";
    }

    verify() {
        let isValidCaptch = this.captchaString == this.userInput;
        if (!this.userInput) {
            this.setError("Enter captcha");
        } else if (!isValidCaptch) {
            this.setError("Not valid captcha");
        }
        if (isValidCaptch)
            this.refreshCaptcha();
        return isValidCaptch;
    }
}
