import {createApp} from 'vue';
import vForm from "./vue-components/vForm";
import vInput from "./vue-components/vInput";
import vSubmit from "./vue-components/vSubmit";
import vAlert from "./vue-components/vAlert";

require('./utils/Form');

window.vue_app = createApp({
    data() {
        return {
            auth_user: {}
        }
    },
    mounted: function () {
        this.$el.parentElement.style.visibility = 'visible';
    }
});

vue_app.component('v-form', vForm);
vue_app.component('v-input', vInput);
vue_app.component('v-submit', vSubmit);
vue_app.component('v-alert', vAlert);

