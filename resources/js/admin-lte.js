require("admin-lte");

const resizer = () => {
    let fixed_wrapper = ".hd-fixed-wrapper";
    let parent_wrapper = $(fixed_wrapper).parent();
    if (parent_wrapper.length > 0 && parent_wrapper[0].style.minHeight != "") {
        $(fixed_wrapper).css({
            height: parent_wrapper[0].style.minHeight,
            overflow: "auto"
        });
    }
}

$(window).on("load", function () {
    resizer();
});

$(window).on("resize", function () {
    resizer();
});
