@extends("layouts.admin.main")
@section('title')
    <title>Account Settings</title>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md"></div>
            <div class="col-md-9 my-5">
                <div class="card p-4">
                    <div class="card-body">
                        <h4 class="text-muted">
                            <span class="right-underline-title">Account Settings</span>
                        </h4>
                        <p class="text-sm text-muted mt-3">
                            Edit your personal contact information.
                            <b>Please ensure all information is correct and accurate. This information is for internal
                                use only and will not be visible on verification pages.</b>
                        </p>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Account Information</label>
                            </div>
                        </div>
                        <v-form action="{{route("admin.profile.update")}}"
                                method="put"
                                clear="false"
                                :data="userDetails">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v-input
                                            :form="userDetails"
                                            field="firstname"
                                            label="First Name"
                                            class="form-control"
                                            autofocus="true"
                                            autocomplete="false"
                                            placeholder="Enter firstname"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v-input
                                            :form="userDetails"
                                            field="lastname"
                                            label="Last Name"
                                            class="form-control"
                                            placeholder="Enter lastname"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v-input
                                            :form="userDetails"
                                            field="email"
                                            label="Email"
                                            class="form-control"
                                            placeholder="Enter email"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <v-input
                                            :form="userDetails"
                                            field="slug"
                                            label="Username"
                                            class="form-control"
                                            placeholder="Enter username"/>
                                    </div>
                                </div>
                            </div>
                            <v-alert :form="userDetails"></v-alert>
                            <div class="row">
                                <div class="col-12">
                                    <v-submit
                                        :form="userDetails"
                                        class="btn btn-primary btn-flat">
                                        Update
                                    </v-submit>
                                </div>
                            </div>
                        </v-form>
                    </div>
                </div>
            </div>
            <div class="col-md"></div>
        </div>
    </div>
@endsection
@section('js')
    <script !src="">
        vue_app.mixin({
            data() {
                return {
                    userDetails: new vForm({
                        firstname: null,
                        lastname: null,
                        email: null,
                        slug: null
                    }, [], false),
                }
            },
            watch: {
                userDetails: {
                    handler: function (value, oldValue) {
                        let response = value.server_response;
                        if (response.user) {
                            this.auth_user = response.user;
                        }
                    },
                    deep: true
                }
            },
            mounted: async function () {
                await this.getProfileDetails();
            },
            methods: {
                getProfileDetails: async function () {
                    try {
                        const response = await axios.get(`{{route('admin.logged.user')}}`);
                        this.userDetails.setData(response.data.user);
                    } catch (e) {
                        console.log(e.response);
                    }
                }
            }
        });
    </script>
@endsection
