<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1">
    @yield('meta')
    <link rel="icon"
          href="{{asset('imgs/favicon.png')}}">
    <title>
        @yield('title')
    </title>
    <link rel="stylesheet"
          href="{{asset('css/app.css')}}">
    @yield('css')
</head>
<body>
@include('layouts.frontend.header')
@yield('content')
@include('layouts.frontend.footer')
<script src="{{asset('js/app.js')}}"></script>
@yield('js')
</body>
</html>
