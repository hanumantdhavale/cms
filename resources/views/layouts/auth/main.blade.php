<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible"
          content="ie=edge">
    <link rel="icon"
          href="{{asset('imgs/favicon.png')}}">
    @yield('meta')
    @yield('title')
    <link rel="stylesheet"
          href="{{asset('css/app.css')}}">
    <link rel="stylesheet"
          href="{{asset('css/admin-lte.css')}}">
    @yield('css')
</head>
<body class="hold-transition login-page">
@include('layouts.auth.header')
<div class="login-box"
     id="vue-app">
    <div class="login-logo">
        <a href="{{route("home")}}">{{env('APP_NAME')}}</a>
    </div>
    @yield('content')
</div>
@include('layouts.auth.footer')
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/admin-lte.js')}}"></script>
<script src="{{asset('js/vue.js')}}"></script>
@yield('js')
<script !src="">
    if (vue_app)
        vue_app.mount("#vue-app");
</script>
</body>
</html>
