<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible"
          content="ie=edge">
    <link rel="icon"
          href="{{asset('imgs/favicon.png')}}">
    @yield('meta')
    @yield('title')
    <link rel="stylesheet"
          href="{{asset('css/app.css')}}">
    <link rel="stylesheet"
          href="{{asset('css/admin-lte.css')}}">
    @yield('css')
</head>
<body class="sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed accent-teal"
      style="height: auto;">
<div class="wrapper"
     id="vue-app"
     style="visibility: hidden;">
    @include('layouts.admin.header')
    @include('layouts.admin.sidebar')
    <div class="content-wrapper">
        <div class="hd-fixed-wrapper">
            @yield('content')
        </div>
    </div>
    @include('layouts.admin.footer')
</div>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/admin-lte.js')}}"></script>
<script src="{{asset('js/vue.js')}}"></script>
@yield('js')
<script !src="">

    vue_app.mixin({
        data: function () {
            return {
                logout_busy: false
            }
        },
        mounted: async function () {
            await this.getUserDetails();
        },
        methods: {
            doLogout: async function () {
                this.logout_busy = true;

                this.logout_busy = false;

            },
            getUserDetails: async function () {
                const response = await axios.get(`{{route('admin.logged.user')}}`);
                this.auth_user = response.data.user;
            }
        }
    });

    if (vue_app)
        vue_app.mount("#vue-app");
</script>
</body>
</html>
