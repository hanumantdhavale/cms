<aside class="main-sidebar sidebar-light-primary">
    <a href="{{route("admin.home")}}"
       class="brand-link navbar-white text-center">
        <img style="width: 65px;max-height: 40px;"
             src="{{asset('imgs/favicon.png')}}"
             alt="{{env("APP_NAME", "")}}">
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-legacy"
                data-widget="treeview"
                role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{route("admin.home")}}"
                       class="nav-link {{in_array(request()->route()->getName(), ['admin.home'])?"active":null}}">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route("admin.profile")}}"
                       class="nav-link {{in_array(request()->route()->getName(), ['admin.profile'])?"active":null}}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Profile
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route("admin.change.password")}}"
                       class="nav-link {{in_array(request()->route()->getName(), ['admin.change.password'])?"active":null}}">
                        <i class="nav-icon fas fa-lock"></i>
                        <p>
                            Change Password
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
