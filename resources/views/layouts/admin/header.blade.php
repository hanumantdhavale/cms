<nav class="main-header navbar navbar-expand border-bottom-0 navbar-light navbar-white">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link"
               data-widget="pushmenu"
               href="JavaScript:void(0)"
               role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            @{{ auth_user.firstname ?? null }}
            <button type="button"
                    :disabled="logout_busy"
                    @click="doLogout"
                    class="btn btn-sm btn-flat btn-outline-danger">
                @{{ logout_busy?"Signing Out...":"Logout" }}
            </button>
        </li>
    </ul>
</nav>
