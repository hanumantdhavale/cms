@extends("layouts.auth.main")
@section('title')
    <title>Login : {{env('APP_NAME')}}</title>
@endsection
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <v-form action="{{route("auth.check")}}"
                    method="post"
                    :data="loginForm">
                <div class="form-group">
                    <v-input :form="loginForm"
                             field="email"
                             label="Email"
                             autofocus="true"
                             class="form-control"
                             placeholder="Enter email"/>
                </div>
                <div class="form-group">
                    <v-input :form="loginForm"
                             type="password"
                             field="password"
                             label="Password"
                             class="form-control"
                             placeholder="*******"/>
                </div>
                <v-alert :form="loginForm"></v-alert>
                <div class="row">
                    <div class="col-12">
                        <v-submit :form="loginForm"
                                  class="btn btn-primary btn-block btn-flat">
                            Sign In
                        </v-submit>
                    </div>
                </div>
            </v-form>
            <p class="mb-0 mt-2">
                <a href="{{route("auth.forget")}}">
                    I forgot my password
                </a>
            </p>
            <p class="mb-0 mt-2">
                <a href="{{route("auth.register")}}">
                    Don't have an account
                </a>
            </p>
        </div>
    </div>
@endsection
@section('js')
    <script !src="">
        vue_app.mixin({
            data() {
                return {
                    loginForm: new vForm({
                        email: null,
                        password: null
                    })
                }
            },
            watch: {
                loginForm: {
                    handler: function (value, oldValue) {
                        let error = this.loginForm.errors.server;
                        if (error && error.response && error.response.status == 419) {
                            this.loginForm.password = null;
                        }
                    },
                    deep: true
                }
            }
        });
    </script>
@endsection
