@extends("layouts.auth.main")
@section('title')
    <title>Set New Password : {{env('APP_NAME')}}</title>
@endsection
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Set new password</p>
            <v-form action="{{route("auth.reset.password")}}"
                    method="post"
                    :data="resetForm">
                <div class="form-group">
                    <v-input :form="resetForm"
                             field="password"
                             label="New Password"
                             autofocus="true"
                             type="password"
                             class="form-control"
                             placeholder="********"/>
                </div>
                <div class="form-group">
                    <v-input :form="resetForm"
                             field="confirm_password"
                             label="Confirm Password"
                             type="password"
                             class="form-control"
                             placeholder="********"/>
                </div>
                <v-alert :form="resetForm"></v-alert>
                <div class="row">
                    <div class="col-12">
                        <v-submit :form="resetForm"
                                  class="btn btn-primary btn-block btn-flat">
                            Set Password
                        </v-submit>
                    </div>
                </div>
            </v-form>
            <p class="mb-0 mt-2">
                <a href="{{route("auth.login")}}">
                    Back to login
                </a>
            </p>
        </div>
    </div>
@endsection
@section('js')
    <script !src="">
        vue_app.mixin({
            data() {
                return {
                    resetForm: new vForm({
                        a: `{{request('a')}}`,
                        t: `{{request('t')}}`,
                        password: null,
                        confirm_password: null
                    })
                }
            }
        });
    </script>
@endsection
