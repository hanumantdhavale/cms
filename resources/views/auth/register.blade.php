@extends("layouts.auth.main")
@section('title')
    <title>Create Your Account : {{env('APP_NAME')}}</title>
@endsection
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <v-form action="{{route("auth.store")}}"
                    method="post"
                    :data="registerForm">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v-input
                                :form="registerForm"
                                field="firstname"
                                label="First Name"
                                class="form-control"
                                autofocus="true"
                                autocomplete="false"
                                placeholder="Enter firstname"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <v-input
                                :form="registerForm"
                                field="lastname"
                                label="Last Name"
                                class="form-control"
                                placeholder="Enter lastname"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <v-input
                                :form="registerForm"
                                field="email"
                                label="Email"
                                class="form-control"
                                placeholder="Enter email"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <v-input
                                :form="registerForm"
                                type="password"
                                field="password"
                                label="Password"
                                class="form-control"
                                placeholder="*******"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <v-input
                                :form="registerForm"
                                type="password"
                                field="confirm_password"
                                label="Confirm Password"
                                class="form-control"
                                placeholder="*******"/>
                        </div>
                    </div>
                </div>
                <v-alert :form="registerForm"></v-alert>
                <div class="row">
                    <div class="col-12">
                        <v-submit
                            :form="registerForm"
                            class="btn btn-primary btn-block btn-flat">
                            Sign Up
                        </v-submit>
                    </div>
                </div>
            </v-form>
            <p class="mb-0 mt-2">
                <a href="{{route("auth.login")}}">
                    I have an account
                </a>
            </p>
        </div>
    </div>
@endsection
@section('js')
    <script !src="">
        vue_app.mixin({
            data() {
                return {
                    registerForm: new vForm({
                        firstname: null,
                        lastname: null,
                        email: null,
                        password: null,
                        confirm_password: null
                    })
                }
            }
        });
    </script>
@endsection
