@extends("layouts.auth.main")
@section('title')
    <title>Forgot Password : {{env('APP_NAME')}}</title>
@endsection
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Recover your account.</p>
            <v-form action="{{route("auth.reset.link")}}"
                    method="post"
                    :data="forgetForm">
                <div class="form-group">
                    <v-input :form="forgetForm"
                             field="email"
                             label="Email"
                             autofocus="true"
                             class="form-control"
                             placeholder="Enter email"/>
                </div>
                <v-alert :form="forgetForm"></v-alert>
                <div class="row">
                    <div class="col-12">
                        <v-submit :form="forgetForm"
                                  class="btn btn-primary btn-block btn-flat">
                            Get Reset Link
                        </v-submit>
                    </div>
                </div>
            </v-form>
            <p class="mb-0 mt-2">
                <a href="{{route("auth.login")}}">
                    Back to login
                </a>
            </p>
        </div>
    </div>
@endsection
@section('js')
    <script !src="">
        vue_app.mixin({
            data() {
                return {
                    forgetForm: new vForm({
                        email: null
                    })
                }
            }
        });
    </script>
@endsection
