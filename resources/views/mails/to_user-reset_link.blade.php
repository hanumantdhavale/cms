@extends('mails.layouts.template-1')
@section('title')
    <title>Message from {{env('APP_NAME', 'NO NAME')}}</title>
@endsection
@section('content')
    <p style="font-size: 14px">
        Hi {{$user->firstname}},
    </p>
    <p style="font-size: 14px">
        Click on below link to reset your password.
        <br>
        <a href="{{$link}}">{{$link}}</a>
    </p>
@endsection
