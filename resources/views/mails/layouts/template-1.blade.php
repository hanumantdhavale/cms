<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type"
          content="text/html;charset=UTF-8">
    @yield('title')
</head>
<body style="font-family: sans-serif;font-family: sans-serif;margin: 0;">
<div
    style="box-sizing: border-box;padding: 20px 30px;border-radius: 5px;margin: 40px auto;border: 1px solid #eee;background: #fff;width: 100%; max-width: 650px; color: #222; font-weight: 500;">
    <table style="width: 100%;">
        <tr>
            <td>
                <span style="padding: 6px 0;display: block; text-align: center">
                    <img style="width: 150px; max-width: 150px; margin: auto;"
                         src="{{asset('imgs/logo-mail.png')}}"
                         alt="{{env('APP_NAME','No App Name')}}">
                </span>
            </td>
        </tr>
        <tr>
            <td>
                @yield('content')
            </td>
        </tr>
        <tr>
            <td>
                <div style="border-bottom: 1px solid #eee; margin: 10px 0;"></div>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0 0 5px 0; text-align: left; font-size: 13px;color: #666;">
                    Kind Regards,
                    <br>
                    <br>
                    {{env("APP_NAME")}} team
                    <br>
                    <a href="{{env("APP_URL")}}">{{env("APP_URL")}}</a>
                    <br>
                    <a href="mail-to:{{env("MAIL_SUPPORT")}}">{{env("MAIL_SUPPORT")}}</a>
                </p>
                <p style="margin: 15px 0 5px 0; text-align: center; font-size: 13px;color: #666;">
                    ***This is an automated email. Replies to this mailbox are not monitored***
                </p>
            </td>
        </tr>
    </table>
</div>
</body>
</html>
