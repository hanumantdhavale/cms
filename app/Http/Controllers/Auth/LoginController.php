<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthLoginRequest;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login()
    {
        return view("auth.login");
    }

    public function check(AuthLoginRequest $request)
    {
        auth()->attempt($request->only("email", "password"));

        if (!auth()->check())
            return response()->json([
                'message' => 'Invalid credentials'
            ], 419);

        if (!auth()->user()->status) {
            auth()->logout();
            return response()->json([
                'message' => 'Your account is blocked/inactive, Please contact support team.'
            ], 419);
        }

        $next = route("admin.home");

        return response()->json([
            'next' => $next,
            'message' => 'User account created successfully!'
        ], 200);
    }
}
