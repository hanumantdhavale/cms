<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthResetRequest;
use App\Mail\ResetedPassword;
use App\Models\ForgetPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ResetController extends Controller
{
    public function reset()
    {
        return view('auth.reset');
    }

    public function set(AuthResetRequest $request)
    {
        $user_id = !empty($request->a) ? decrypt($request->a) : null;
        $token = !empty($request->t) ? decrypt($request->t) : null;

        if (!$forgetPassword = ForgetPassword::where(['user_id' => $user_id, 'token' => $token])->first())
            return response()->json([
                'message' => 'Invalid link, Please create new & try again.'
            ], 400);

        User::find($forgetPassword->user_id)->update($request->only('password'));
        $forgetPassword->delete();
        Mail::to($forgetPassword->user)->send(new ResetedPassword($forgetPassword->user));

        return response()->json(['message' => 'Your account password reset, Please login with new password.'], 200);
    }
}
