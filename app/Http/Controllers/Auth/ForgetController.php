<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthForgetRequest;
use App\Models\User;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class ForgetController extends Controller
{
    public function forget()
    {
        return view("auth.forget");
    }

    public function resetLink(AuthForgetRequest $request)
    {
        try {

            $token = uniqid();
            $user = User::where('email', $request->email)->first();
            $user->forgetPassword()->create(['token' => $token]);

            return response()->json([
                'message' => "Password reset link sent to your email address.",
                "user" => $user
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }
}
