<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register()
    {
        return view("auth.register");
    }

    public function store(AuthRegisterRequest $request)
    {
        User::create($request->only("firstname", "lastname", "email", "password", "confirm_password"));
        auth()->attempt($request->only("email", "password"));
        $next = auth()->check() ? route("admin.home") : route("auth.login");
        return response()->json([
            'next' => $next,
            'message' => 'User account created successfully!'
        ], 200);
    }
}
