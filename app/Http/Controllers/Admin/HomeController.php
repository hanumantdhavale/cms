<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view("admin.home");
    }

    public function logout()
    {
        auth()->logout();
        return response()->json([
            'next' => route("auth.login")
        ], 200);
    }
}
