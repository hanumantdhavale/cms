<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminProfileRequest;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view('admin.profile');
    }

    public function show()
    {
        $user = auth()->user();
        return response()->json(['user' => $user]);
    }

    public function update(AdminProfileRequest $request)
    {
        $user = auth()->user();
        $user->update($request->only('firstname'));
        return response()->json(['user' => $user, 'message' => 'User profile details updated successfully!'], 200);
    }
}
