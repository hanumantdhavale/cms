<?php

namespace App\Observers;

use App\Models\ForgetPassword;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class ForgetPasswordObserver
{
    /**
     * @param ForgetPassword $forgetPassword
     */
    public function creating(ForgetPassword $forgetPassword)
    {
        if (request()->has('email')) {
            $user = User::where(request()->only('email'))->first();
            ForgetPassword::where(['user_id' => $user->id])->delete();
        }
    }

    /**
     * Handle the ForgetPassword "created" event.
     *
     * @param \App\Models\ForgetPassword $forgetPassword
     * @return void
     */
    public function created(ForgetPassword $forgetPassword)
    {
        $link = route("auth.reset", ["a" => encrypt($forgetPassword->user_id), 't' => encrypt($forgetPassword->token)]);
        Mail::to($forgetPassword->user)->send(new \App\Mail\ForgetPassword($forgetPassword->user, $link));
    }

    /**
     * Handle the ForgetPassword "updated" event.
     *
     * @param \App\Models\ForgetPassword $forgetPassword
     * @return void
     */
    public function updated(ForgetPassword $forgetPassword)
    {
        //
    }

    /**
     * Handle the ForgetPassword "deleted" event.
     *
     * @param \App\Models\ForgetPassword $forgetPassword
     * @return void
     */
    public function deleted(ForgetPassword $forgetPassword)
    {
        //
    }

    /**
     * Handle the ForgetPassword "restored" event.
     *
     * @param \App\Models\ForgetPassword $forgetPassword
     * @return void
     */
    public function restored(ForgetPassword $forgetPassword)
    {
        //
    }

    /**
     * Handle the ForgetPassword "force deleted" event.
     *
     * @param \App\Models\ForgetPassword $forgetPassword
     * @return void
     */
    public function forceDeleted(ForgetPassword $forgetPassword)
    {
        //
    }
}
