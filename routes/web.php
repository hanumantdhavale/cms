<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ForgetController;
use App\Http\Controllers\Auth\ResetController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\PasswordController;

Route::get("/", function () {
    return "Home Page";
})->name("home");

Route::name("auth.")
    ->middleware('auth.guest')
    ->prefix("auth")->group(function () {
        Route::get("register", [RegisterController::class, 'register'])->name("register");
        Route::post("register", [RegisterController::class, 'store'])->name("store");
        Route::get("login", [LoginController::class, 'login'])->name("login");
        Route::post("login", [LoginController::class, 'check'])->name("check");
        Route::get("forget", [ForgetController::class, 'forget'])->name("forget");
        Route::post("forget", [ForgetController::class, 'resetLink'])->name("reset.link");
        Route::get("reset", [ResetController::class, 'reset'])->name("reset");
        Route::post("reset", [ResetController::class, 'set'])->name("reset.password");
    });

Route::name("admin.")
    ->middleware("auth.guard")
    ->prefix("admin")
    ->group(function () {
        Route::get("/", [HomeController::class, "index"])->name("home");
        Route::get("/auth/user", [ProfileController::class, "show"])->name("logged.user");
        Route::get("/profile", [ProfileController::class, "index"])->name("profile");
        Route::put("/profile", [ProfileController::class, "update"])->name("profile.update");
        Route::get("/change-password", [PasswordController::class, "index"])->name("change.password");
        Route::post("/logout", [HomeController::class, "logout"])->name("logout");
    });
